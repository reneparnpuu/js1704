import { Component } from '@angular/core';
import { Http } from '@angular/http';
import {Post} from "./post";


@Component({
    selector: 'my-app',
    templateUrl: 'app/app.html'
})
export class AppComponent {
    posts: Post[];

    constructor(private http: Http) {
        http.get('api/posts')
            .toPromise()
            .then(response => {
                this.posts = response.json()
                    .map((each :any) => new Post(each.title, each.text));
            });
    }

}
