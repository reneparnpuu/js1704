'use strict';

const mongodb = require('mongodb');
const Dao = require('./dao.js');
const ObjectID = mongodb.ObjectID;

var url = 'mongodb://...';

var data = { name: 'Jill' };

var database;
mongodb.MongoClient.connect(url).then(db => {
    database = db;
    return db.collection('test-collection').insertOne(data);
}).then(() => {
    closeDb(database)
}).catch(error => {
    closeDb(database);
    throw error;
});

function closeDb(database) {
    if (database) {
        database.close();
    }
}