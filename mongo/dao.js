'use strict';

const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;
const COLLECTION = 'test-collection';

class Dao {

    connect(url) {
        return mongodb.MongoClient.connect(url)
            .then(db => this.db = db);
    }

    findAll() {
        return this.db.collection(COLLECTION).find().toArray();
    }

    close() {
        if (this.db) {
            this.db.close();
        }
    }
}

module.exports=Dao;
