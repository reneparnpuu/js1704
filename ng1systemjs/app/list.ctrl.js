Ctrl.$inject = ['data'];

function Ctrl(data) {

    var vm = this;
    vm.posts = [];

    init();

    function init() {
        data.getPosts().then(function (result) {
            vm.posts = result;
        });
    }

}

module.exports = Ctrl;