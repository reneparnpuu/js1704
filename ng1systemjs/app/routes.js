Conf.$inject = ['$routeProvider'];

function Conf($routeProvider) {

    $routeProvider.when('/list', {
        templateUrl : 'app/list.html',
        controller : 'ListCtrl',
        controllerAs : 'vm'
    }).when('/new', {
        templateUrl : 'app/add.html',
        controller : 'AddCtrl',
        controllerAs : 'vm'
    }).otherwise('/list');
}

module.exports = Conf;