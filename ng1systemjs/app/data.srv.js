Srv.$inject = ['$q'];

function Srv($q) {

    var srv = this;

    srv.addPost = addPost;
    srv.getPosts = getPosts;

    var posts = [
        {
            title: "Post 1"
        },
        {
            title: "Post 2"
        }
    ];

    function addPost(post) {
        posts.push(post);

        return $q.resolve();
    }

    function getPosts() {
        return $q.resolve(posts);
    }

}

module.exports = Srv;