var app = angular
    .module('app', ['ngRoute'])
    .config(require('./routes.js'))
    .service('data', require('./data.srv.js'))
    .controller('AddCtrl', require('./add.ctrl.js'))
    .controller('ListCtrl', require('./list.ctrl.js'))
;

angular.element(document).ready(function() {
    angular.bootstrap(document, ['app']);
});
