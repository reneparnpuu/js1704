Ctrl.$inject = ['$location', 'data'];

function Ctrl($location, data) {

    var vm = this;
    vm.title = '';
    vm.text = '';
    vm.addNew = addNew;

    function addNew() {
        var newPost = {
            title : vm.title,
            text : vm.text
        };

        data.addPost(newPost).then(function (a) {
            $location.path('/list');
        });
    }
}

module.exports = Ctrl;
