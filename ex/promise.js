

var returnOne = () => {
    $http.get('http://localhost:3000/posts').then(function (result) {
        vm.posts = result.data;
    });
};

var promise = new Promise(returnOne);

//promise.then(result => { console.log(result);

returnOne().then(result => {
    return 1;
}).then(result => {
    // no return
}).then(result => {
    throw new Error();
}).then(result => {
    // skipped
}).catch(error => {
    // process error
    // throw error
    // return undefined
}).then(result => {
    // executed if catch
    //   does not throw
});

