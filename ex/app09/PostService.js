(function () {
    'use strict';

    var app = angular.module('app');
    app.service('PostService', PostService);
    app.constant('POSTS_SESSION_STORAGE_KEY', 'posts');

    function PostService ($http, $q, $window, POSTS_SESSION_STORAGE_KEY) {
        var service = this;

        var postsFromSessionStorage = $window.sessionStorage.getItem(POSTS_SESSION_STORAGE_KEY);
        this.data = postsFromSessionStorage ? JSON.parse(postsFromSessionStorage) : [];

        this.getPost = function () {
            if (service.data.length > 0) {
                return $q.resolve(service.data);
/*              // This should work in Angular 2
                return new Promise(function () {
                    console.log(service.data);
                    return service.data;
                });
                */
            }

            return $http.get('http://localhost:3000/posts').then(function (result) {
                var data = result.data;

                service.data = data;

                $window.sessionStorage.setItem(POSTS_SESSION_STORAGE_KEY, JSON.stringify(data));

                return data;
            });
        };

        this.addPost = function (newPost) {
            return $http.post('http://localhost:3000/api/posts', newPost).then(function () {
                return service.invalidateData();
            });
        };

        this.deletePost = function (postId) {
            return $http.delete('http://localhost:3000/posts/' + postId).then(function () {
                return service.invalidateData();
            });
        };

        this.invalidateData = function () {
            service.data = [];
        }
    }

})();