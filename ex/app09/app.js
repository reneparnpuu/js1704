(function () {
    'use strict';

    var app = angular.module('app', ['ngRoute']);

    app.directive('post', getDeleteButton);

    function getDeleteButton() {
       return {
           templateUrl: 'app09/post.dir.html'
       };
    }

})();