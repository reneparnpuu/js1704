(function () {
    'use strict';

    angular.module('app').controller('AddCtrl', Ctrl);

    function Ctrl($http, $location, PostService) {

        var vm = this;
        vm.title = '';
        vm.text = '';
        vm.addNew = addNew;

        function addNew() {
            var newPost = {
                title : vm.title,
                text : vm.text
            };

            PostService.addPost(newPost).then(function () {
                $location.path('/list');
            });
        }

    }

})();
