(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ListCtrl', Ctrl);

    // $ muutujad nagu $http on angulari enda omad, ilma on enda teenused.
    function Ctrl($http, PostService) {

        var vm = this;
        vm.posts = [];
        vm.deletePost = deletePost;

        init();

        function init() {
            PostService.getPost().then(function (result) {
                vm.posts = result;
            });
        }

        function deletePost(postId) {
            PostService.deletePost(postId).then(init);
        }
    }
})();
