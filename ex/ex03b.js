'use strict';

var request = require("request");

// helper function
function get(url, cb) {
    request('http://localhost:3000' + url, function(error, response, body) {
        var data = JSON.parse(body);
        cb(data);
    });
}

get('/api/persons/2948', function(data) {
    var person = {
        name : '',
        isInsured : null,
        taxDebt : null
    };

    person.name = data.name;
    get('/api/health-insurance/' + data.code, function(data) {
        person.isInsured = data.isInsured;
        get('/api/tax-debt/' + data.id, function (data) {
            person.taxDebt = data.debt;
            console.log(person);
        });
    });
});

