(function () {
    'use strict';

    angular.module('app').controller('C1', Controller1);

    function Controller1($http) {
        var vm = this;

        vm.items = [];
        vm.inputVar = '';

        getPosts();

        vm.addItem = function () {
            var newItem = {
                title : vm.inputVar,
                done : false
            };

            $http.post('http://localhost:3000/posts', newItem).then(getPosts);
        };

        function getPosts() {
            $http.get('http://localhost:3000/posts').then(function(result) {
                vm.items = result.data;
            });
        }
    }
})();
