'use strict';

var array = [1, 2, 3, 4, 5, 6];

array.push(7, 8);

array = array
    .filter(function(value) {
        return value % 2 === 0;
    })
    .map(function(value) {
        return value * value;
    })
;

console.log('Even numbers squared: ' + array);