'use strict';

function Person(name) {
   this.name = name;

   this.getName = function () {
       return this.name;
   };
}

var person = new Person('Jack');
console.log(person.getName());


//-----------------------------------------
function Isik(name) {
    this.name = name;
}

var isik = new Isik('Jane Doe');
Isik.prototype.getName = function () {
    return this.name;
};

console.log(isik.getName());