(function () {
    'use strict';

    angular.module('app').config(Conf);

    function Conf($routeProvider) {

        $routeProvider.when('/list', {
            templateUrl : 'app07_routes/list.html',
            controller : 'ListCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app07_routes/add.html',
            controller : 'AddCtrl',
            controllerAs : 'vm'
        }).otherwise('/list');
    }

})();