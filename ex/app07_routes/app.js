(function () {
    'use strict';

    var app = angular.module('app', ['ngRoute']);

    function getPosts($http) {
        $http.get('http://localhost:3000/posts').then(function(result) {
            vm.items = result.data;
        });
    }
})();