(function () {
    'use strict';

    angular.module('app').controller('ListCtrl', ListCtrl);

    function ListCtrl($http) {
        var vm = this;

        vm.items = [];

        getPosts($http);

        function getPosts($http) {
            $http.get('http://localhost:3000/posts').then(function(result) {
                vm.items = result.data;
            });
        }
    }
})();
