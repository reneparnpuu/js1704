(function () {
    'use strict';

    angular.module('app').controller('AddCtrl', AddCtrl);

    function AddCtrl($http) {
        var vm = this;

        vm.inputVar = '';

        vm.addItem = function () {
            var newItem = {
                title : vm.inputVar,
                done : false
            };

            $http.post('http://localhost:3000/posts', newItem).then(getPosts($http));
        };
    }
})();