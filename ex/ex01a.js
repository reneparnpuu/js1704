'use strict';

var array = [1, 2, 3, 4, 5, 6];

var oddNumbers = filter(array, function (each) {
    return each % 2 === 1;
});

console.log('Odd numbers: ' + oddNumbers);

function filter(list, predicate) {
    return list.filter(predicate);
}
