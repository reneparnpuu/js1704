export class Telefon {

    private number: string;

    constructor(number: string) {
        this.number = number;
    }

    public getNumber(): string {
        return this.number;
    };
}