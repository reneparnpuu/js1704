"use strict";
var Isik = (function () {
    function Isik(nimi, telefon) {
        this.nimi = nimi;
        this.telefon = telefon;
    }
    Isik.prototype.saySomething = function () {
        return 'My name is ' + this.nimi + ' and phoneNr is ' + this.telefon.getNumber();
    };
    return Isik;
}());
exports.Isik = Isik;
//# sourceMappingURL=isik.js.map