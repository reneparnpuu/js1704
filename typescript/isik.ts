import {Telefon} from "./telefon";

export class Isik {
    private nimi : string;
    private telefon : Telefon;

    constructor(nimi: string, telefon: Telefon) {
        this.nimi = nimi;
        this.telefon = telefon;
    }

    public saySomething() : string {
        return 'My name is ' + this.nimi + ' and phoneNr is ' + this.telefon.getNumber();
    }
}
