(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ListCtrl', Ctrl);

    // Hoiab ära uglify ja DI probleemi, kus teeb data lühemaks nimeks, mille tõttu DI katki läheks.
    Ctrl.$inject = ['data'];

    function Ctrl(data) {

        var vm = this;
        vm.posts = [];

        init();

        function init() {
            data.getPosts().then(function (result) {
                vm.posts = result;
            });
        }

    }

})();
