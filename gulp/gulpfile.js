var gulp = require('gulp');
var bowerFiles = require('main-bower-files');
var inject = require('gulp-inject');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var angularFilesort = require('gulp-angular-filesort');
var del = require('del');
var q = require('q');

gulp.task('default', ['copy-partials'], function () {

    var vendorScripts = gulp.src(bowerFiles('**/*.js')) // files
        .pipe(concat('vendor.min.js')) // Filename
        .pipe(uglify()) // min
        .pipe(gulp.dest('dist')); // where file is put!

    var appScripts = gulp.src('src/app/**/*.js')
            .pipe(angularFilesort()) // sorts to right order
            .pipe(sourcemaps.init()) // extra file for seeing beauty code (non minimized)
            .pipe(concat('app.min.js')) // filename
            .pipe(uglify()) // minimize
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('dist')); // where file is put

    return gulp
        .src('src/index.html')
        .pipe(gulp.dest('dist')) // write first to get relative path for inject
        .pipe(inject(vendorScripts, {relative: true, name: 'bower'}))
        .pipe(inject(appScripts, { relative: true }))
        .pipe(gulp.dest('dist'));

});

gulp.task('copy-partials', ['clean'], function() {
    return gulp.src('src/app/**/*.html', { base: './src' })
        .pipe(gulp.dest('dist'));
});

gulp.task('clean', function() {
    var deferred = q.defer();
    del('dist', function() {
        deferred.resolve();
    });
    return deferred.promise;
});
